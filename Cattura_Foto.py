# Crivellaro Diego

import sys
import io
import time
from flask import Flask, Response, request

try:
  import picamera
except:
  # linea di uscita
  sys.exit(0)

app = Flask(__name__)

@app.route('/scatto', methods=['GET'])
def capture_photo():
  try:
    with picamera.PiCamera() as camera:
      camera.resolution = (1024, 768)
      # A seconda del montaggio della picamera uso h/v flip:
      # camera.hflip = True
      # camera.vflip = True
      # Anteprima e pausa camera
      camera.start_preview()
      time.sleep(0.1) # preparo la picamera
      stream = io.BytesIO()
      # cattura una immagine in formato jpeg, immagine come flusso di byte in memoria:
      camera.capture(stream, format='jpeg', use_video_port=True)
      # recupero flusso:
      stream.seek(0)
  except:
    return 'Errore: camera non disponibile!', 503
  # invio immagine(image) al browser (Chrome/Safari):
  return Response(stream.read(), mimetype='image/jpeg')


if __name__ == '__main__':
  app.run(host='0.0.0.0', debug=True)