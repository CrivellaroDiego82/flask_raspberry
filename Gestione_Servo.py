# Crivellaro Diego

import RPi.GPIO as GPIO
from flask import Flask, render_template
import os

PIN_SERVO1 = 4
PIN_SERVO2 = 17
PIN_SERVO3 = 18
PIN_SERVO4 = 21
PIN_SERVO5 = 22
PIN_SERVO6 = 23
PIN_SERVO7 = 24
PIN_SERVO8 = 25


# Pin Setup:
GPIO.setmode(GPIO.BCM) # Broadcom Schema Pin
GPIO.setwarnings(False) #Escludo Messaggi
GPIO.setup(PIN_SERVO1,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO2,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO3,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO4,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO5,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO6,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO7,  GPIO.OUT) # PWM output
GPIO.setup(PIN_SERVO8,  GPIO.OUT) # PWM output


srv1PWM = GPIO.PWM(PIN_SERVO1,22 )
srv2PWM = GPIO.PWM(PIN_SERVO2,22 )
srv3PWM = GPIO.PWM(PIN_SERVO3,22 )
srv4PWM = GPIO.PWM(PIN_SERVO4,22 )
srv5PWM = GPIO.PWM(PIN_SERVO5,22 )
srv6PWM = GPIO.PWM(PIN_SERVO6,22 )
srv7PWM = GPIO.PWM(PIN_SERVO7,22 )
srv8PWM = GPIO.PWM(PIN_SERVO8,22 )


srv1PWM.start(0)
srv2PWM.start(0)
srv3PWM.start(0)
srv4PWM.start(0)
srv5PWM.start(0)
srv6PWM.start(0)
srv7PWM.start(0)
srv8PWM.start(0)

app = Flask(__name__)

@app.route("/")
def drive1():
    return render_template("servo.html")

@app.route("/avanti")
def avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "forward"

@app.route("/indietro")
def indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "back"

@app.route("/ser1avanti")
def ser1avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 1 Avanti"

@app.route("/ser1indietro")
def ser1indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 1 Indietro"

@app.route("/ser2avanti")
def ser2avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 2 Avanti"

@app.route("/ser2indietro")
def ser2indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 2 Indietro"

@app.route("/ser3avanti")
def ser3avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 3 Avanti"

@app.route("/ser3indietro")
def ser3indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 3 Indietro"

@app.route("/ser4avanti")
def ser4avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 4 Avanti"

@app.route("/ser4indietro")
def ser4indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 4 Indietro"

@app.route("/ser5avanti")
def ser5avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 5 Avanti"

@app.route("/ser5indietro")
def ser5indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 5 Indietro"

@app.route("/ser6avanti")
def ser6avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(5)
    return "Servo 6 Avanti"

@app.route("/ser6indietro")
def ser6indietro():
    srv1PWM.ChangeDutyCycle(5)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 6 Indietro"

@app.route("/ser7avanti")
def ser7avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 7 Avanti"

@app.route("/ser7indietro")
def ser7indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 7 Indietro"

@app.route("/ser8avanti")
def ser8avanti():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 8 Avanti"

@app.route("/ser8indietro")
def ser8indietro():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "Servo 8 Indietro"

@app.route("/stop")
def stop():
    srv1PWM.ChangeDutyCycle(0)
    srv1PWM.ChangeDutyCycle(0)
    return "stop"

@app.route("/sinistra")
def sinistra():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "left"

@app.route("/destra")
def destra():
    srv1PWM.ChangeDutyCycle(1)
    srv1PWM.ChangeDutyCycle(1)
    return "right" 
   
if __name__ == "__main__":

   app.run(host='10.0.2.30', port=7000, debug=True)